# /* 
#  * Written by Guillaume Voisin 2019 , JBCA, The University of Manchester ; LUTh, Observatoire de Paris, PSL Research University (guillaume.voisin@manchester.ac.uk, guillaume.voisin@obspm.fr astro.guillaume.voisin@google.com)
#  *
#  */

############ COMPILER OPTIONS #########################
CXX = g++   # put here your version of c++ compiler
MPICXX = mpic++ # put here your version of mpic++
PYTHON = python3

## Should not have to be changed
CXXFLAGS = -g -Wall -O2 -fopenmp # compiling options
MPICXXFLAGS = -g


########## LINKING VARIABLES ######################################################
# TEMPO2 contains an include directory
T2 = $(TEMPO2)
# TEMPO2 static libraries
LIBSTATICTEMPO2 = ./
########################################################

## LINKING VARIABLES that should not be edited.
LDFLAGS = -Bstatic  -I$(T2)/include/ -L$(LIBSTATICTEMPO2)  -ltempo2 -lsofa -ldl
####################################################################################


sources  =  MCMC4Tempo.cpp Utilities_MCMC_affinv.cpp Utilities_MPI.cpp Utilities.cpp acc.cpp

headers = Tempo2_interface.h Utilities_MPI.h Utilities_MCMC_affinv.h Utilities.h acor.h


all : MCMC4Tempo New_parfile_from_MCMC

MCMC4Tempo : $(headers) $(sources)
	$(MPICXX) -D MPIMODE -fopenmp -std=gnu++11 -D MPIMODE $(MPICXXFLAGS) -o $@ $(sources) $(LDFLAGS)

New_parfile_from_MCMC : New_parfile_from_MCMC.cpp Tempo2_interface.h Utilities.cpp
	$(CXX) -std=gnu++11 $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

t2interface : Tempo2_interface_test.cpp Tempo2_interface.h Utilities.cpp
	$(CXX) -std=gnu++11 $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

clean :
	rm -f *.o *.gch  *.exe *.so
	rm -rf build/
