/* Written by Guillaume Voisin 2017 -2019 ,
* JBCA, The University of Manchester, UK (guillaume.voisin@manchester.ac.uk)
* LUTh, Observatoire de Paris, PSL Research University (guillaume.voisin@obspm.fr astro.guillaume.voisin@google.com)
*
* This files provides test functions for the MCMC algorithm. 
*/
#ifndef MCMC_lnposterior_functions_h
# define MCMC_lnposterior_functions_h

#ifdef MPIMODE
    #include "mpi.h"
#endif
#include <iostream>
#include <chrono>
#include <random>
#include <cfloat>
#include "Utilities.h"
#include <stdio.h>
#include <string.h>

#include "Fittriple.h"


class fctgaussienne
{
    int ndim ;
public:
    int verbose=0;

    fctgaussienne(int number_of_dim){ndim =number_of_dim;};
    double operator()(double* pars)
    {
        double res = 0.;
        double inter = 0.;
//          res -= pow(pars[0] - 1000.*pars[1],2);
//          res -= pow(pars[0] + pars[1],2);
        for (int i = 0; i < ndim ; i++)
        {
            inter = pow(pars[i] - 0.2 * static_cast<double>(i),2);
            res -= inter / static_cast<double>(i+1) ;
        }
        return res;
    };
    void Print_MCMC()
    {
        printf("Fonction Gaussienne in %d dimensions! \n", ndim);
    };
};



class fctsimudata
{
    double std ;
    int ndim=1;
    int ndata;
    double efac = 1.;
    double * ts ;
    double ** posterior_cormat;
public:
    double * data;
    double ** coeffs;
    int verbose=0;
    int errorscale=0; // activate the hyperparameter error scale, as the las parameter of pars

    fctsimudata()
    {
    };

    void external_data(double * simudata, double ** coefficients, int numberofdata, int ndimensions, double stddev)
    {
      int i,j;
      ndata = numberofdata;
      data = (double*) malloc(sizeof(double) * ndata);
      std = stddev;
      ndim = ndimensions;
      coeffs = (double **)malloc(sizeof(double*) * ndata);
      for (i =0 ; i < ndata ; i ++) coeffs [i] = (double*) malloc(sizeof(double) * ndim);
      for (i =0 ; i < ndata ; i ++)
      {
        data[i] = simudata[i];
        for (j =0 ; j < ndim ; j ++) coeffs[i][j] =  coefficients[i][j];
      }
    }

    void generate_data( int numberofdata,  double * parameters, double stddev, int ndimensions, double basefreq, double timespan)
    {
        int i,j;
        double mean ;
        ndata = numberofdata;
        data = (double*) malloc(sizeof(double) * ndata);
        std = stddev;
        ndim = ndimensions;
        ts = (double *) malloc(sizeof(double) * ndata);
        coeffs = (double **)malloc(sizeof(double*) * ndata);
        for (i =0 ; i < ndata ; i ++) coeffs [i] = (double*) malloc(sizeof(double) * ndim);
        for (i =0 ; i < ndata ; i ++)
        {
          ts[i] = timespan / ndata * i;
          for (j =0 ; j < ndim ; j ++) coeffs[i][j] =  cos(j*basefreq * ts[i]);
        }
            // Random generator init
        // obtain a seed from the system clock:
        unsigned seed1 = chrono::system_clock::now().time_since_epoch().count();
        mt19937 randomgen(seed1);  // mt19937 is a standard mersenne_twister_engine
        normal_distribution<double> normaldist(0.,std); // used for initialization

        for ( i = 0; i < ndata ; i++)
        {
          mean = 0.;
          for (j = 0; j < ndim  ; j ++ ) mean += parameters[j] *coeffs[i][j];
          data[i] = mean + normaldist(randomgen);
        }
        double var = 0;
        for (i = 0; i < ndata ; i++) var += (data[i] - mean)*(data[i] - mean);
        var /= ndata;
        var = sqrt(var);
        printf("--------var %f %f %f\n", var, var/ std, std );
      //  std /=1.2;
        printf("std = %f\n\n", std);

// Compute correlation matrix of the posterior distribution
        int k,l;
        posterior_cormat = (double **)malloc(sizeof(double*) * ndata);
        for (i =0 ; i < ndim ; i ++) posterior_cormat[i] = (double*) malloc(sizeof(double) * ndim);
        for (k =0 ; k < ndim ; k ++)
        {
          for (l =0 ; l < ndim ; l ++)
          {
            posterior_cormat[k][l] = 0;
            for (i = 0; i < ndata; i ++) posterior_cormat[k][l] += coeffs[i][k] * coeffs[i][l] ;
            posterior_cormat[k][l] /=   std * std;
          };
        };

// Save Intial conditions and posterior correlation
        int mpirank = 0;
        char  filename[50];
        sprintf(filename,"simudata.txt");
#ifdef MPIMODE
        MPI_Comm_rank(MPI_COMM_WORLD,&mpirank);
#endif
        if (mpirank == 0)
        {
          sprintf(filename,"simu-data.txt");
          Savetxt(filename, data, ndata);
          sprintf(filename,"simu-times.txt");
          Savetxt(filename, ts, ndata);
          sprintf(filename,"simu-model_coeffs.txt");
          Savetxt(filename, coeffs, ndata, ndim);
          sprintf(filename,"simu-posterior_correlation_matrix.txt");
          Savetxt(filename, posterior_cormat, ndim, ndim);
        }
        free(ts);
        for (i =0 ; i < ndim ; i ++) free(posterior_cormat [i]);
        free(posterior_cormat);
    };

    ~fctsimudata()
    {
      int i = 0;
      free(data);
      for (i =0 ; i < ndata ; i ++) free(coeffs [i]);
      free(coeffs);
    };

    double operator()(double* pars, double beta)
    {
        double parfinal[ndim+1];
        memcpy(parfinal, pars, sizeof(double) * ndim);
        if (errorscale >0)
        {
            if (beta != 1.)
            {
                parfinal[ndim]=0.;
                pars[ndim] =efac -1.;
            }
            else
            {
                parfinal[ndim] = (pars[ndim]);
            }
        }

        return lnposterior(parfinal) * beta;
    }


    double operator()(double* pars)
    {
        return lnposterior(pars);
    }

    double lnposterior(double * pars)
    {

        double res = 0.;
        double inter = 0.;
        double currentefac = 1.;
        double mean ;
        int j ;

        if (errorscale >0) currentefac = 1. + pars[ndim];

        res = 0.;
        for (int i = 0; i < ndata ; i++)
        {
            mean = 0.;
            for (j = 0; j < ndim  ; j ++ ) mean += pars[j] *coeffs[i][j];
            res -= 0.5*(data[i] - mean)*(data[i] - mean)/( std*std * currentefac *currentefac);
        }
        res -= ndata * log(currentefac);

//         printf("std efac %f %f \n", std, efac);

        return res;
    };


    double Swap_temperatures(double * params_lowtemp, double & lnposterior_lowtemp, const double beta_lowtemp, double * params_hightemp, double & lnposterior_hightemp, const double beta_hightemp )
    {

        int locndim = ndim;
        double walkerbuffer[locndim];
        double lnposteriory = lnposterior_lowtemp;
        double efac_temp1 = 0.;
        double efac_lowtemp = 1.;
        double efac_hightemp = 1.;
        double logefac =0.;


        int iefac =-1;
        if (errorscale >0)
        {
            iefac = 1;
            locndim +=1;
        }

        if (iefac >= 0)
        {
            efac_temp1 = 1 + params_lowtemp[iefac];
            efac_lowtemp = 1 + params_lowtemp[iefac];
            efac_hightemp = 1 + params_hightemp[iefac];
            logefac = ndata* log(efac_lowtemp / efac_hightemp);
        }

        lnposterior_lowtemp = lnposterior_hightemp * beta_lowtemp / beta_hightemp;
        lnposterior_hightemp = lnposteriory * beta_hightemp / beta_lowtemp;

        memcpy(walkerbuffer, params_lowtemp, locndim*sizeof(double));
        memcpy(params_lowtemp, params_hightemp, locndim*sizeof(double));
        memcpy(params_hightemp, walkerbuffer, locndim*sizeof(double));

        if (beta_lowtemp == 1. and iefac >= 0)
        {
            lnposterior_lowtemp += logefac;
            lnposterior_hightemp -= beta_hightemp * logefac;
            params_hightemp[iefac] = efac- 1.;
            params_lowtemp[iefac] = efac_temp1 -1.;
        }
    };


    double Get_chi2(const double lnposterior, const double * params, const double beta)
    // Return the chi2 based on the log of the posterior probability density, therefore removing any efac contribution.
    // Does not recompute the lnposterior : fast
    // Remove effect of temperature. ( i.e. gives chi2 with temperature = 1)
    {
        int iefac = 1;
        double currentefac =1.;
        if (errorscale >0 ) currentefac = 1. + params[iefac];

        return -(lnposterior/beta - ndata*log(currentefac));
    };



    void Print_MCMC()
    {
        printf("Fonction Simu Data in %d dimensions! \n", ndim);

    };

    void Get_parameter_name(int paramnumber, char * name)
    {
        sprintf(name, "p%i", paramnumber);
    }

    double initial_distribution(mt19937& random_generator, int param_number)
    {
        normal_distribution<double> initial_distribution(0.,0.0001);

        return initial_distribution(random_generator);
    }


    int Get_ndim() // return number of dimensions
    {
        return ndim;
    };

};


class fctmultigaussienne
{
    int ndim ;
    int npeaksperdim;
    double stdmainpeak=1.;
    double stdpeaksec;
    double peakstep ;
public:
    int verbose=0;
    int errorscale=0; // activate the hyperparameter errror scale, as the las parameter of pars

    fctmultigaussienne(int number_of_dim, int half_number_of_peaks_per_dim)
    {
        ndim =number_of_dim;
        npeaksperdim=half_number_of_peaks_per_dim;
        stdpeaksec = stdmainpeak / (4.* npeaksperdim);
        peakstep = stdmainpeak;
    };

    double operator()(double* pars, double beta)
    {
        double parfinal[ndim+1];
        memcpy(parfinal, pars, sizeof(double) * ndim);
        if (errorscale >0)
        {
            if (beta != 1.)
                parfinal[ndim]=1.;
            else
                parfinal[ndim] = (1.+ pars[ndim]);
        }

        return lnposterior(parfinal) * beta;
    }


    double operator()(double* pars)
    {
        return lnposterior(pars);
    }

    double lnposterior(double * pars)
    {
        double res = 0.;
        double inter = 0.;
        int k ;

//         printf("pars %.5e %.5e \n", pars[0], pars[1]);

        for (int i = 0; i < ndim ; i++)
        {
            for (int j = -npeaksperdim ; j <= npeaksperdim ; j++)
            {
                if (j != 0)
                {
                    inter = 0.;
                    for (k = 0; k < ndim ; k++)
                    {
                        if (k == i )
                            inter += -pow((pars[i] - peakstep*j)/stdpeaksec,2)*0.5;
                        else
                            inter += -pow((pars[k])/stdpeaksec,2) * 0.5;
//                         printf("i %d j %d k %d inter %.5e \n", i, j, k, inter);
                    }
                    res += exp(inter);
//                     printf("i %d j  %d exp(inter) %.5e \n", i, j, exp(inter));
                }
            }
            inter = 0.;
            for (k = 0; k < ndim ; k++) inter += -pow((pars[k])/stdmainpeak,2) *0.5;
            //res += exp(inter);
        }
        res = log(res);
        if (errorscale >0)
        {
                res /= pars[ndim] * pars[ndim];
                res += log(pars[ndim]);
        }
        return res;
    };

    double Get_chi2(const double lnposterior, const double * params, const double beta)
    {
      return lnposterior;
    };

    void Print_MCMC()
    {
        printf("Fonction Multi Gaussienne in %d dimensions! \n", ndim);
        printf("npeaksperdim %d stdmainpeak %.5e stdpeaksec %.5e peakstep %.5e \n", npeaksperdim, stdmainpeak, stdpeaksec, peakstep);
    };

    double initial_distribution(mt19937& random_generator, int param_number)
    {
        normal_distribution<double> initial_distribution(0.,0.0001);

        return initial_distribution(random_generator);
    }

    void Get_parameter_name(int paramnumber, char * name)
    {

        sprintf(name, "%i", paramnumber);
    }

    int Get_ndim() // return number of dimensions
    {
        return ndim;
    };



    double Swap_temperatures(double * params_lowtemp, double & lnposterior_lowtemp, const double beta_lowtemp, double * params_hightemp, double & lnposterior_hightemp, const double beta_hightemp )
    {
      printf("Swap_temperatures NOT IMPLEMENTED !");
    };


};


#endif
