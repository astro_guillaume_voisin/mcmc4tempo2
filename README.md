# Repository moved to 
https://gitlab.obspm.fr/gvoisin/mcmc4tempo2

## License : 
MCMC4tempo © 2019 by Guillaume Voisin is licensed under CC BY-NC-SA 4.0 
https://creativecommons.org/licenses/by-nc-sa/4.0/ 
Guillaume Voisin, LUTh, Observatoire de Paris, PSL Research University, CNRS (guillaume.voisin@obspm.fr astro.guillaume.voisin@google.com)

## What ?
This code provides a parallelized, affine-invariant MCMC for Tempo2 with the possibility of using parallel tempering.

This program samples a distribution using the algorithm described in Goodman & Weare 2010 (GW10), Communitcations in Applied mathematics and Computational Sciences, Vol5, Issue 1
The parallelization scheme is from : Foreman-Mackey et al., 2013, Pulications of the Astronomical Society of the Pacific, Vol 125, Issue 925

Used in :
    - " PSR J2051-0827 companion's quadrupole]{First measurement of the gravitational quadrupole moment of a blackwidow companion in PSR J2051-0827", Voisin et al., MNRAS, 2019 (DOI: 10.1093/mnras/staa953, HAL: https://hal.archives-ouvertes.fr/hal-02427662, Arxiv : https://arxiv.org/abs/2004.01564)

## How to cite : 
Please cite " PSR J2051-0827 companion's quadrupole]{First measurement of the gravitational quadrupole moment of a blackwidow companion in PSR J2051-0827", Voisin et al., MNRAS, 2019 (DOI: 10.1093/mnras/staa953, HAL: https://hal.archives-ouvertes.fr/hal-02427662, Arxiv : https://arxiv.org/abs/2004.01564)

## Who ? 
Written by Guillaume Voisin 2017-2019 ,
LUTh, Observatoire de Paris, PSL Research University, CNRS (guillaume.voisin@obspm.fr astro.guillaume.voisin@google.com)

## How to compile and install ?
- gcc version > 4.8 should allow compilation without problem.
- Edit the links in the makefile as appropriate for your configuration. The key element is to link to the tempo2 libraries which must have been compiled with the gcc -fpic option for shared libraries. This can be done with the following substitions in Tempo2's Makefile (tested on Tempo2 2018) :

sed -i 's/CFLAGS = -g -O2 -Wno-error/CFLAGS = -g -O2 -Wno-error -fpic/g' Makefile

sed -i 's/CPPFLAGS = -I/CPPFLAGS = -I -fpic/g' Makefile

sed -i 's/CXXFLAGS = -g -O2/CXXFLAGS = -g -O2 -fpic/g' Makefile

Then, recompile tempo2. The static libraries compiled with fpic are stored in the tempo2_install_directory/lib/. You can either copy or link them in the mcmc4tempo2 directory, or modifify mcmc4tempo2's makefile accordingly.

- Do "make"

## How to use it ? 
- Note : parallel tempering not implemented in Tempo2_interface.h

- ./MCMC4Tempo will prompt a brief summary of the syntax as follow 
:

./MCMC4Tempo parfile datafile fileout number_of_walkers_per_processor number_of_walker_set_moves frequency_of_chain_extension number_of_proc_per_node stat_and_save_frequency
ments :

  parfile : Tempo2 parfile

  datafile : Tempo2 timfile

  fileout : prefix of the name of the file containing the chain. The full filename is of the form fileout--temp=X.XX.res where X.XX is the temperature. If parallel tempering is used, then one file is created for each temperature.

  half_number_of_walkers_per_processor: each computing core computes 2 *  half_number_of_walkers_per_processor walkers

  number_of_walker_set_moves: total number of iterations

  frequency_of_chain_extension : number of iteration between two extension of the chain (i.e. recording of the state of chain) with the current walkers

  number_of_proc_per_node : number of cores on one node of a computer cluster. (if using a single node/compute/laptop just write the number of cores you want to use)

  stat_and_save_frequency : number of iterations between computing and displaying statistics, and saving the chain to file.


* Note on total number of walkers :
    The number of walkers of a run is 2 *  half_number_of_walkers_per_processor * number_of_cores where number_of_cores is 1 if run in monoprocessor mode and specified with the mpirun command otherwise. If running on several nodes, specifying number_of_proc_per_node allows a (much) faster initialization, but may lead to bugs if incorrect.

* Optional keyword options :

  turnfile <filename> : inapplicable to Tempo2

  previouschain <filename_prefix> : to restart from a previous run which file is named according to <filename_prefix>--temp=X.XX.res

  recordrefused : enables the recording of candidate walkers to which a jump has been refused. Useful for diagnostics

  aparam <value> : value of the parameter "a" of Goodman and Weare (2010) that characterises the proposal distribution. Default is 2.

  targettemp <float value> : Temperature of the run, lowest temperature if parallel tempering is enabled. Default is 1.

  tempswapfreq <int value> : Number of iterations between two attempts to swap walkers between temperatures

  ntemp <int value> : Number of parallel temperatures to run. Default is 1.

  tempfactor <float value> <float value> ... : list of ratios of Tn+1/Tn where Tn are the parallel temperatures. There should be ntemp-1. If there is only one then the same ratio is applied everywhere, otherwise there needs to be ntemp - 1 values given. Default ratio is sqrt(2) (see e.g. Earl and Deem 2005, DOI 10.1039/b509983h).

  nproc_per_temperature <int value> : For parallelisation of temperature. Gives the number of processors dedicated to one temperature. 

  --sieving <float value> : remove the walkers for which Best_log_posterior - log(posterior) > value. Removal happens every stat_and_save_frequency iterations. Help removing stuck walkers. To use with care. 

  --diagnostics : save some extra files for diagnostics


- New_parfile_from_MCMC : creates a tempo2 parfile from the MCMC chain produced with MCMC4Tempo

Syntax is : 'New_parfile_from_MCMC parfile timfile MCMC_result_file new_parfile key'

  key : what parameters to extract from the MCMC chain result. Can only be mean 'mean' for now.


## Examples :
    For the paper " PSR J2051-0827 companion's quadrupole]{First measurement of the gravitational quadrupole moment of a blackwidow companion in PSR J2051-0827" the command used was :

mpirun -n 24 ./MCMC4Tempo J2051-0827_ELL11-postfit.par J2051-0827_all.tim mcmcpt-J2051_ELL11-1 5 10000 10 30 50 > log-run1 &
