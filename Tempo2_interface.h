// Written by Guillaume Voisin, 2019
// email : guillaume.voisin@manchester.ac.uk ;  astro.guillaume.voisin@gmail.com
//
// Interface between Tempo2 and MCMC4tempo
#ifndef tempo2_interface
# define tempo2_interface


#include <iostream>
#include <chrono>
#include <random>
#include <cfloat>
#include <stdio.h>
#include <string.h>
#include "tempo2.h"
#include "t2fit.h"
#include "Utilities.h"

using namespace std ;

class fctTempo2
{
  const double sec2microsec = 1000000.;

  int tempo2_noWarnings;
  int tempo2_npsr ;
  pulsar *tempo2_psr;

  int fitted_parameter_map[param_LAST][2];
  int nfit;

  const int removeMean = 1;

public :
    int verbose=0;
    double beta =1.; // 1/Temperature


    double lastlnlikelyhood;
//    double lastprior;


    fctTempo2()
    {
      tempo2_psr = NULL;
    };



    ~fctTempo2()
    {
      destroyOne(tempo2_psr);
      free(tempo2_psr);
    };

    void Load(char * parfile, char * timfile)
    {
      char * covarFuncFile=NULL;
      char ** commandline = NULL;
      MAX_PSR = 1;
      MAX_OBSN = 100000;
      tempo2_npsr = 1;
      tempo2_psr = (pulsar *)realloc(tempo2_psr, sizeof(pulsar)*MAX_PSR);
      if (tempo2_psr == NULL) printf(" \n *** Error : memory allocation failed in Load for tempo2_psr !\n ");

      char t2parfile[tempo2_npsr][MAX_FILELEN];
      char t2timfile[tempo2_npsr][MAX_FILELEN];
      strcpy(t2parfile[0],parfile);
      strcpy(t2timfile[0],timfile);

      tempo2_noWarnings = 1;
      initialise(tempo2_psr,tempo2_noWarnings); // SINON SEGFAULT !!!
      readParfile(tempo2_psr,t2parfile,t2timfile,tempo2_npsr);
      readTimfile(tempo2_psr,t2timfile,tempo2_npsr);
      preProcess(tempo2_psr,tempo2_npsr,0,commandline);
      veryFast = 0; // tempo2 global variable
      formBatsAll(tempo2_psr,tempo2_npsr);
      //t2fit_prefit(tempo2_psr,tempo2_npsr);
      //t2Fit(tempo2_psr,tempo2_npsr,covarFuncFile);

      int ifit = 0;
      int iparam =0;
      int i = 0;
      for (i = 0 ; i < param_LAST ; i ++)
      {
        for (iparam = 0 ; iparam < tempo2_psr[0].param[i].aSize ; iparam++)
        {
          if (tempo2_psr[0].param[i].fitFlag[iparam] >= 1)
          {
            fitted_parameter_map[ifit][0] = i;
            fitted_parameter_map[ifit][1] = iparam;
            ifit +=1;
          }
        }
      }
      nfit = ifit;
    }

    int Get_ndim() // return number of dimensions
    {
        return nfit;
    };


    void Print_MCMC()
    {
        int ntoas = tempo2_psr[0].nobs;
        double chi2 = 0;
        char pname[100];
        long double params[Get_ndim()] ;

        for (int i = 0; i < Get_ndim() ; i++) params[i] = tempo2_psr[0].param[fitted_parameter_map[i][0]].val[fitted_parameter_map[i][1]];

        Set_parameters(params);
        Compute_lnposterior();

        printf("Beta = 1/temperature = %.5e \n",beta);
        chi2 = Get_chi2(lastlnlikelyhood, params, 1.);
        printf("Initial chi2 /(ntoas - nparams) is %.8f, and chi2 = %.8f\n", chi2/(ntoas-nfit), chi2);
        printf("Initial residual RMS = %.8f microsec.\n", Residual_RMS());

        printf("\nParameter index -> (tempo2 parameter label, element) Parameter name  Initial value Error \n");
        for (int i=0; i < nfit ; i++)
        {
          Get_parameter_name(i, pname);
          printf("%i -> (%i, %i) %s %.15Le %.15Le \n ", i, fitted_parameter_map[i][0], fitted_parameter_map[i][1], pname, Get_parameter_value(i), Get_parameter_error(i) );
        }
        printf("]\n");
    };

    void Get_parameter_name(int paramnumber, char * name)
    {

        strcpy(name, tempo2_psr[0].param[fitted_parameter_map[paramnumber][0]].label[fitted_parameter_map[paramnumber][1]]);
    }

    long double Get_parameter_value(int paramnumber)
    {
        return tempo2_psr[0].param[fitted_parameter_map[paramnumber][0]].val[fitted_parameter_map[paramnumber][1]];
    }


    long double Get_parameter_error(int paramnumber)
    {
        return  tempo2_psr[0].param[fitted_parameter_map[paramnumber][0]].prefitErr[fitted_parameter_map[paramnumber][1]];
    }



    double operator()(long double * params)
    {
        Set_parameters(params);
        return Compute_lnposterior();
    };

    double operator()(long double * params, const double betatemp)
    {
      Set_parameters(params);
      return Compute_lnposterior() * betatemp;
    };


    void Set_parameters(long double * params)
    // Set new parameters into tempo2 but does not compute anything. Run Compute_lnposterior after.
    {
      int ifit = 0;

      for (ifit = 0; ifit < nfit ; ifit ++)
      {
         tempo2_psr[0].param[fitted_parameter_map[ifit][0]].val[fitted_parameter_map[ifit][1]] = params[ifit];
      }

    };

    double Compute_lnposterior()
    // Compute lnposterior with current parameters. Use Set_parameters to change the parameters.
    {
        updateBatsAll(tempo2_psr,tempo2_npsr);  // Ne pas utiliser formBatsAll sinon fuite de mémoire !!! (probablement causé par l'appel à clock_corrections(psr,npsr);   ou ephemeris_routines(psr,npsr); ou extra_delays(psr,npsr);  ...
        formResiduals(tempo2_psr, tempo2_npsr, removeMean);

        lastlnlikelyhood = 0.;

        for (int i = 0; i < tempo2_psr[0].nobs ; i++)
        {
          lastlnlikelyhood -= 0.5 * pow(static_cast<double>(tempo2_psr[0].obsn[i].residual) *sec2microsec / tempo2_psr[0].obsn[i].toaErr,2) ;
        }

        return (lastlnlikelyhood ) ;
    };

    void Swap_temperatures(long double * params_lowtemp, double & lnposterior_lowtemp, const double beta_lowtemp, long double * params_hightemp, double & lnposterior_hightemp, const double beta_hightemp )
    {
      printf("Error : Temperature swap not implemented in Tempo2_interface.h !!");
    };

    double Get_chi2(const double lnposterior, const long double * params, const double beta)
    // Return the chi2 based on the log of the posterior probability density, therefore removing any efac contribution.
    // Does not recompute the lnposterior : fast
    // Remove effect of temperature. ( i.e. gives chi2 with temperature = 1)
    {
        return -2.*(lnposterior/beta);
    };


    long double initial_distribution(mt19937& random_generator, int param_number)
    {
        normal_distribution<long double> inidistro(0.,0.0001);
        long double err = Get_parameter_error(param_number); //tempo2_psr[0].param[fitted_parameter_map[param_number][0]].err[fitted_parameter_map[param_number][1]];
        long double val = Get_parameter_value(param_number); //tempo2_psr[0].param[fitted_parameter_map[param_number][0]].val[fitted_parameter_map[param_number][1]];
        long double rnumber  = inidistro(random_generator);
        return  rnumber * err + val ;
    }




// ---------------- Methods not necessary for interface with MCMC --------------


    fctTempo2(char * parfile, char * timfile)
    {
      Load(parfile, timfile);
    };


    double operator()() // Return the chi2 of the current parameters
    {
      return Compute_lnposterior();
    };


    double Residual_RMS()
    {
      double mean =0.;
      double rms = 0.;
      double totweight=0.;

      for (int i = 0; i < tempo2_psr[0].nobs ; i++)
      {
        mean += static_cast<double>(tempo2_psr[0].obsn[i].residual) *sec2microsec / static_cast<double>(tempo2_psr[0].obsn[i].toaErr);
        rms += pow(static_cast<double>(tempo2_psr[0].obsn[i].residual) *sec2microsec,2) /  static_cast<double>(tempo2_psr[0].obsn[i].toaErr);
        totweight += 1./ static_cast<double>(tempo2_psr[0].obsn[i].toaErr);
      }
      mean /= totweight;
      rms /= totweight;
      return sqrt(rms - mean*mean);
    }

    void Save_residuals(char * filename)
    {
      double ** residuals;
      int nobs = tempo2_psr[0].nobs ;
      int i = 0;
      residuals = (double**) malloc(sizeof(double*)* nobs);
      for ( i = 0; i < nobs ; i++) residuals[i]= (double*) malloc(sizeof(double)*2);

      for (int i = 0; i < tempo2_psr[0].nobs ; i++)
      {
        residuals[i][0] = static_cast<double>(tempo2_psr[0].obsn[i].sat);
        residuals[i][1] = static_cast<double>(tempo2_psr[0].obsn[i].residual *sec2microsec );
      }
       Savetxt(filename, residuals, nobs, 2 );
       for ( i = 0; i < nobs ; i++) free(residuals[i]);
       free(residuals);
    }

    void Save_parfile(char * filename) // Write a new parfile at filename
    {
      textOutput(tempo2_psr,tempo2_npsr,0,0,0,1,filename);
    }
};



#endif
