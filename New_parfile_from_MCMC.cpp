// Written by Guillaume Voisin, 2019
// email : guillaume.voisin@manchester.ac.uk ;  astro.guillaume.voisin@gmail.com
//
// Writes a new Tempo2 parfile using a MCMC chain result generated by MCMC4Tempo.
#include <iostream>
#include <chrono>
#include <random>
#include <cfloat>
#include <stdio.h>
#include <string.h>
#include "Tempo2_interface.h"

int main ( int argc, char *argv[] )
{
    if (argc != 6)
    {
      printf("Syntax is : 'New_parfile_from_MCMC parfile timfile MCMC_result_file new_parfile key'\n");
      printf("key : what parameters to extract from the MCMC chain result. Can only be mean 'mean' for now\n\n");
      return 1;
    }
    fctTempo2 t2interf = fctTempo2();
    t2interf.Load(argv[1], argv[2]);
    printf("\n Loaded parfile '%s' and timfile '%s' with chi2 = %.5e \n\n", argv[1], argv[2], t2interf());
    int ndim = t2interf.Get_ndim();
    long double params[ndim];

    //------------------ Load chain
    long double ** chain ;
    int i , j;
    FILE * myfile ;
    myfile = fopen(argv[3], "r") ;
    int fndim=0;
    int fchain_freq =0;
    int fnb_walkers =0;
    int fchain_size =0;
    if (myfile == NULL )
    {
      printf("Could not read MCMC result file.");
      return 1;
    }
    fscanf(myfile, "#  %i  %i  %i %i\n",&fndim, &fnb_walkers, &fchain_size, &fchain_freq);
    if (ndim != fndim)
    {
        printf("\n Error ! Chain in file %s and desired chain have different number of dimensions : %i against %i \n\n", argv[3], fndim, ndim);
        return 1 ;
    }
    chain = (long double**) malloc(( fchain_size) * sizeof(long double*) );
    for (i =0  ; i < (fchain_size) ; i++)
    {
        chain[i] = (long double*) malloc((ndim+1)*sizeof(long double));
        for (j= 0 ; j < ndim + 1 ; j++) chain[i][j] = 0.;
    }

    for(i=0 ; i < fchain_size ; ++i)
    {
        if ( fscanf(myfile, "%Le    ", &(chain[i][0]) ) == 1)
        {
          for (j = 1; j < fndim ; ++j ) {
               fscanf(myfile, "%Le    ", &(chain[i][j]) );
           }
           fscanf(myfile, "%Le    \n", &(chain[i][fndim]) );
       } else // if read fails line is skipped
       {
         i -= 1;
         fnextline(myfile);
        // printf("\n Skipping line %i\n",i);
       }
    }
    if (i < fchain_size)
    {
      printf("\nWarning : only %i/%i lines could be read from file %s\n", i, fchain_size, argv[3]);
      fchain_size = i;
    }
    // for(i=0 ; i < fchain_size ; ++i) {
    //     for (j = 0; j < fndim ; ++j ) {
    //          fscanf(myfile, "%le    ", &(chain[i][j]) );
    //      }
    //      fscanf(myfile, "%le    \n", &(chain[i][fndim]) );
    //  }
    //  fclose(myfile);
     // ------------------ End of loading chain

     if (strcmp(argv[5],"mean") == 0) // Calculate mean of the chain
     {
       for (j = 0 ; j < ndim ; j ++) params[j] = 0.;
       for (i = 0; i < fchain_size; i ++)
       {
         for (j = 0 ; j < ndim ; j ++)
         {
           params[j] += chain[i][j];
         }
       }
       for (j = 0 ; j < ndim ; j ++) params[j] /= fchain_size;
     }
     else
     {
       printf("You need to choose what parameters to create a parfile from ! \n");
       return 1;
     }

     // Apply params and save to the new parfile
     printf("Writing parameters '%s' to parfile at %s\n", argv[5], argv[4]);
     printf("Chi2 = %.3e\n", t2interf(params));
     t2interf.Save_parfile(argv[4]);
     return 0;
}
